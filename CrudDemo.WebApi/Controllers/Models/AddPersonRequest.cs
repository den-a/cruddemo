﻿using System.ComponentModel.DataAnnotations;

namespace CrudDemo.WebApi.Controllers.Models
{
    public class AddPersonRequest
    {
        [Required]
        [EmailAddress]
        [StringLength(256)]
        public string Email { get; set; }
        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(50)]
        public string LastName { get; set; }
        [Required]
        [RegularExpression(@"0[45]\d{8}")]
        public string MobilePhone { get; set; }
    }
}
