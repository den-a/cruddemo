﻿using System.ComponentModel.DataAnnotations;

namespace CrudDemo.WebApi.Controllers.Models
{
    public class UpdatePersonRequest : AddPersonRequest
    {
        [Range(1, long.MaxValue)]
        public long Id { get; set; }
    }
}
