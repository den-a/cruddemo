﻿using System.ComponentModel.DataAnnotations;

namespace CrudDemo.WebApi.Controllers.Models
{
    public class PageRequest
    {
        [Range(1, 100)]
        public int PageSize { get; set; }
        [Range(1, int.MaxValue)]
        public int PageNumber { get; set; }
    }
}
