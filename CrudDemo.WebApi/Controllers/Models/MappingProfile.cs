﻿using AutoMapper;
using CrudDemo.Dal.Entities;

namespace CrudDemo.WebApi.Controllers.Models
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Person, PersonResponse>();
            CreateMap<AddPersonRequest, Person>()
                .ForMember(d => d.Id, o => o.Ignore());
            CreateMap<UpdatePersonRequest, Person>();
        }
    }
}
