﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Bogus;
using CrudDemo.Dal.Models;
using CrudDemo.Dal.Repositories;
using CrudDemo.WebApi.Controllers.Models;
using Microsoft.AspNetCore.Mvc;
using Person = CrudDemo.Dal.Entities.Person;

namespace CrudDemo.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonsController : ControllerBase
    {
        private readonly IPersonRepository _personRepository;
        private readonly IMapper _mapper;

        public PersonsController(IPersonRepository personRepository, IMapper mapper)
        {
            _personRepository = personRepository ?? throw new ArgumentNullException(nameof(personRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet("GetPage")]
        public async Task<ListPage<PersonResponse>> GetPage([FromQuery] PageRequest request)
        {
            var page = await _personRepository.GetPage(request.PageSize, request.PageNumber);

            var response = _mapper.Map<ListPage<PersonResponse>>(page);

            return response;
        }

        [HttpPut]
        public async Task<Person> Put([FromBody] UpdatePersonRequest request)
        {
            var person = _mapper.Map<Person>(request);

            await _personRepository.Update(person);

            return person;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] AddPersonRequest request)
        {
            var person = _mapper.Map<Person>(request);

            await _personRepository.Add(person);

            return new ObjectResult(person) { StatusCode = 201 };
        }

        [HttpDelete("{id}")]
        public Task Delete([FromRoute] int id)
        {
            return _personRepository.Remove(id);
        }

        [HttpPost("Seed/{count}")]
        public async Task Seed([FromRoute] int count)
        {
            var faker = new Faker<Person>()
                .RuleFor(p => p.Email, (f, p) => f.Internet.Email())
                .RuleFor(p => p.FirstName, (f, p) => f.Name.FirstName())
                .RuleFor(p => p.LastName, (f, p) => f.Name.LastName())
                .RuleFor(p => p.MobilePhone, (f, p) => f.Phone.PhoneNumber("04########"));

            for (int i = 0; i < count; i++)
            {
                var person = faker.Generate();
                await _personRepository.Add(person);
            }
        }
    }
}
