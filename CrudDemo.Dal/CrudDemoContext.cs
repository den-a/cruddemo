﻿using CrudDemo.Dal.Entities;
using Microsoft.EntityFrameworkCore;

namespace CrudDemo.Dal
{
    public partial class CrudDemoContext : DbContext
    {
        public CrudDemoContext(DbContextOptions<CrudDemoContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Person> Persons { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>(entity =>
            {
                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.MobilePhone)
                    .HasMaxLength(20);
            });
        }
    }
}
