﻿using CrudDemo.Dal.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CrudDemo.Dal
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddCrudDemoContext(this IServiceCollection services, IConfiguration config)
        {
            services.AddDbContext<CrudDemoContext>(options => options.UseSqlServer(config["ConnectionStrings:CrudDemoConnectionString"]));
            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<IPersonRepository, PersonRepository>();
            return services;
        }
    }
}
