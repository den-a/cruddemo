﻿using System.Threading.Tasks;
using CrudDemo.Dal.Entities;
using CrudDemo.Dal.Models;

namespace CrudDemo.Dal.Repositories
{
    public interface IPersonRepository
    {
        Task Add(Person person);
        Task Update(Person person);
        Task Remove(int id);
        Task<ListPage<Person>> GetPage(int pageSize, int pageNumber);
    }
}
