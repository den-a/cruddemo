﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CrudDemo.Dal.Entities;
using CrudDemo.Dal.Models;
using Microsoft.EntityFrameworkCore;

namespace CrudDemo.Dal.Repositories
{
    public class PersonRepository : IPersonRepository
    {
        private readonly CrudDemoContext _db;

        public PersonRepository(CrudDemoContext db)
        {
            _db = db ?? throw new ArgumentNullException(nameof(db));
        }

        public Task Add(Person person)
        {
            _db.Persons.Add(person);
            return _db.SaveChangesAsync();
        }

        public Task Update(Person person)
        {
            _db.Persons.Update(person);
            return _db.SaveChangesAsync();
        }

        public Task Remove(int id)
        {
            _db.Persons.Remove(new Person {Id = id});
            return _db.SaveChangesAsync();
        }

        public async Task<ListPage<Person>> GetPage(int pageSize, int pageNumber)
        {
            if (pageSize < 1)
            {
                throw new ArgumentException($"{nameof(pageSize)} must be positive");
            }

            if (pageNumber < 1)
            {
                throw new ArgumentException($"{nameof(pageNumber)} must be positive");
            }

            var page = new ListPage<Person>();

            page.TotalCount = await _db.Persons.AsNoTracking().CountAsync();

            page.List = await _db.Persons.AsNoTracking()
                .OrderBy(p => p.FirstName)
                .ThenBy(p => p.LastName)
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();

            return page;
        }
    }
}
