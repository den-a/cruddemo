﻿using System.Collections.Generic;

namespace CrudDemo.Dal.Models
{
    public class ListPage<T>
    {
        public IEnumerable<T> List { get; set; }
        public int TotalCount { get; set; }
    }
}
