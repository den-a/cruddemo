import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ConfirmComponent } from '../components/confirm/confirm.component';

export interface IConfirmOptions {
  text: string;
  title?: string;
  yesText?: string;
  noText?: string;
  yesClass?: string;
  noClass?: string;
}

@Injectable({
  providedIn: 'root'
})
export class ConfirmService {

  constructor(private modalService: NgbModal) { }

  confirm(options: IConfirmOptions): Promise<any> {
    const modalRef = this.modalService.open(ConfirmComponent);
    modalRef.componentInstance.options = Object.assign({
      title: 'Please Confirm',
      yesText: 'Yes',
      noText: 'No',
      yesClass: 'btn-primary',
      noClass: 'btn-secondary',
    }, options);

    return modalRef.result;
  }
}
