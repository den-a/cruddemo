import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Person } from '../models/person';
import { ListPage } from '../models/list-page';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private readonly api = environment.apiPrefix;

  constructor(private httpClient: HttpClient) { }

  getPersons(pageSize: number = 100, pageNumber: number = 1): Observable<ListPage<Person>> {
    return this.httpClient.get<ListPage<Person>>(`${this.api}persons/getpage?pageSize=${pageSize}&pageNumber=${pageNumber}`);
  }

  addPerson(person: Person): Observable<any> {
    return this.httpClient.post(`${this.api}persons`, person);
  }

  updatePerson(person: Person): Observable<any> {
    return this.httpClient.put(`${this.api}persons`, person);
  }

  deletePerson(id: number): Observable<any> {
    return this.httpClient.delete(`${this.api}persons/${id}`);
  }

  seed(count: number = 10): Observable<any> {
    return this.httpClient.post(`${this.api}persons/seed/${count}`, {});
  }
}
