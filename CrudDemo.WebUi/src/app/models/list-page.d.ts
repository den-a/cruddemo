export type ListPage<T> = {
    list: T[];
    totalCount: number;
}
