export type Person = {
    id?: number;
    email: string;
    firstName: string;
    lastName: string;
    mobilePhone: string;
}
