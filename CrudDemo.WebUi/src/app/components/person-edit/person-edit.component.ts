import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ApiService } from 'src/app/services/api.service';
import { Person } from 'src/app/models/person';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-person-edit',
  templateUrl: './person-edit.component.html',
  styleUrls: ['./person-edit.component.scss']
})
export class PersonEditComponent implements OnInit {
  @Input() editMode: boolean;
  @Input() person: Person;
  busy = false;
  failed = false;

  constructor(public activeModal: NgbActiveModal, private apiService: ApiService) { }

  ngOnInit() {
    if (!this.editMode) {
      this.person = {
        email: '',
        firstName: '',
        lastName: '',
        mobilePhone: '',
      };
    }
  }

  onSubmit() {
    this.busy = true;
    this.failed = false;

    let o$: Observable<any>;
    if (this.editMode) {
      o$ = this.apiService.updatePerson(this.person);
    } else {
      o$ = this.apiService.addPerson(this.person);
    }

    o$.subscribe(() => {
      this.busy = false;
      this.activeModal.close();
    }, () => {
      this.busy = false;
      this.failed = true;
    });
  }

  onDismiss() {
    this.activeModal.dismiss();
  }
}
