import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { PersonEditComponent } from './person-edit.component';
import { ApiService } from 'src/app/services/api.service';
import { asyncData, asyncError } from 'src/test';
import { FormsModule } from '@angular/forms';

describe('PersonEditComponent', () => {
  let sut: PersonEditComponent;
  let fixture: ComponentFixture<PersonEditComponent>;
  let apiServiceSpy: jasmine.SpyObj<ApiService>;
  let activeModalSpy: jasmine.SpyObj<NgbActiveModal>;

  beforeEach(async(() => {
    const apiSpy = jasmine.createSpyObj('ApiService', ['addPerson', 'updatePerson']);
    const modalSpy = jasmine.createSpyObj('NgbActiveModal', ['close', 'dismiss']);

    TestBed.configureTestingModule({
      providers: [
        { provide: ApiService, useValue: apiSpy },
        { provide: NgbActiveModal, useValue: modalSpy },
      ],
      declarations: [PersonEditComponent],
      imports: [
        FormsModule,
        NgbModule,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    apiServiceSpy = TestBed.get(ApiService);
    activeModalSpy = TestBed.get(NgbActiveModal);
    fixture = TestBed.createComponent(PersonEditComponent);
    sut = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(sut).toBeTruthy();
  });

  it('ngOnInit() should initialise person when not in edit mode', () => {
    expect(sut.editMode).toBeFalsy();
    expect(sut.person).toBeDefined();
    expect(sut.person.email).toBe('');
    expect(sut.person.firstName).toBe('');
    expect(sut.person.lastName).toBe('');
    expect(sut.person.mobilePhone).toBe('');
  });

  it('onDismiss() should dismiss dialog', () => {
    sut.onDismiss();
    expect(activeModalSpy.dismiss).toHaveBeenCalled();
  });

  it('onSubmit() should set flags correctly at the start', () => {
    expect(sut.busy).toBeFalsy();
    sut.failed = true;
    apiServiceSpy.addPerson.and.returnValue(asyncData({}));

    sut.onSubmit();

    expect(sut.busy).toBeTruthy();
    expect(sut.failed).toBeFalsy();
  });

  it('onSubmit() should add pesrson when not in edit mode', () => {
    apiServiceSpy.addPerson.and.returnValue(asyncData({}));

    sut.onSubmit();

    expect(apiServiceSpy.addPerson).toHaveBeenCalledWith(sut.person);
  });

  it('onSubmit() should update pesrson when in edit mode', () => {
    apiServiceSpy.updatePerson.and.returnValue(asyncData({}));
    sut.editMode = true;

    sut.onSubmit();

    expect(apiServiceSpy.updatePerson).toHaveBeenCalledWith(sut.person);
  });

  it('onSubmit() should close dialog on success', () => {
    apiServiceSpy.addPerson.and.returnValue(asyncData({}));

    sut.onSubmit();

    fixture.whenStable().then(() => {
      fixture.detectChanges();

      expect(activeModalSpy.close).toHaveBeenCalled();
      expect(sut.busy).toBeFalsy();
    });
  });

  it('onSubmit() should not close dialog on error', () => {
    apiServiceSpy.addPerson.and.returnValue(asyncError({}));

    sut.onSubmit();

    fixture.whenStable().then(() => {
      fixture.detectChanges();

      expect(activeModalSpy.close).not.toHaveBeenCalled();
      expect(sut.busy).toBeFalsy();
    });
  });
});
