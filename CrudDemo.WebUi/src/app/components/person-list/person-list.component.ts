import { ConfirmService } from './../../services/confirm.service';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ApiService } from 'src/app/services/api.service';
import { Person } from 'src/app/models/person';
import { PersonEditComponent } from '../person-edit/person-edit.component';
import { ListPage } from 'src/app/models/list-page';

@Component({
  selector: 'app-person-list',
  templateUrl: './person-list.component.html',
  styleUrls: ['./person-list.component.scss']
})
export class PersonListComponent implements OnInit {
  persons: Person[] = [];
  totalCount = 0;
  pageSize = 15;
  pageNumber = 1;
  busy = false;

  constructor(
    private apiService: ApiService,
    private modalService: NgbModal,
    private confirmService: ConfirmService) { }

  ngOnInit() {
    this.onRefresh();
  }

  onRefresh() {
    this.busy = true;

    this.apiService.getPersons(this.pageSize, this.pageNumber).subscribe((page: ListPage<Person>) => {
      this.persons = page.list;
      this.totalCount = page.totalCount;
      this.busy = false;
    }, () => {
      this.busy = false;
    });
  }

  onAdd() {
    const modalRef = this.modalService.open(PersonEditComponent);
    modalRef.componentInstance.editMode = false;

    modalRef.result
      .then(() => this.onRefresh())
      .catch(() => { });
  }

  onEdit(person: Person) {
    const modalRef = this.modalService.open(PersonEditComponent);
    modalRef.componentInstance.editMode = true;
    modalRef.componentInstance.person = Object.assign({}, person);

    modalRef.result
      .then(() => this.onRefresh())
      .catch(() => { });
  }

  onDelete(person: Person) {
    this.confirmService.confirm({
      text: `Are you sure you want to delete ${person.firstName} ${person.lastName}?`,
      yesClass: 'btn-danger',
    }).then(() => {
      this.apiService.deletePerson(person.id).subscribe(() => this.onRefresh()); // TODO: inform on error
    }).catch(() => { });
  }
}
