import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { PersonListComponent } from './person-list.component';
import { ApiService } from 'src/app/services/api.service';
import { asyncData } from 'src/test';
import { ConfirmService } from 'src/app/services/confirm.service';

describe('PersonListComponent', () => {
  let component: PersonListComponent;
  let fixture: ComponentFixture<PersonListComponent>;
  let apiServiceSpy: jasmine.SpyObj<ApiService>;

  beforeEach(async(() => {
    const apiSpy = jasmine.createSpyObj('ApiService', ['getPersons']);
    const confirmSpy = jasmine.createSpyObj('ConfirmService', ['confirm']);


    TestBed.configureTestingModule({
      providers: [
        { provide: ApiService, useValue: apiSpy },
        { provide: ConfirmService, useValue: confirmSpy },
      ],
      declarations: [PersonListComponent],
      imports: [NgbModule]
    }).compileComponents();

    apiServiceSpy = TestBed.get(ApiService);
    apiServiceSpy.getPersons.and.returnValue(asyncData([]));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
