import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IConfirmOptions } from 'src/app/services/confirm.service';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent {
  @Input() options: IConfirmOptions = {} as any;

  constructor(public activeModal: NgbActiveModal) { }

  onYes() {
    this.activeModal.close();
  }

  onNo() {
    this.activeModal.dismiss();
  }
}
