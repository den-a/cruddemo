import { Component, ViewChild } from '@angular/core';

import { ApiService } from 'src/app/services/api.service';
import { PersonListComponent } from './components/person-list/person-list.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @ViewChild('list') list: PersonListComponent;

  constructor(private apiService: ApiService) {}

  onGenerate() {
    this.apiService.seed().subscribe(() => this.list.onRefresh());
  }
}
