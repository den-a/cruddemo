import { TestBed, async } from '@angular/core/testing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { PersonListComponent } from './components/person-list/person-list.component';
import { ApiService } from './services/api.service';

describe('AppComponent', () => {
  beforeEach(async(() => {
    const apiSpy = jasmine.createSpyObj('ApiService', ['getPersons']);

    TestBed.configureTestingModule({
      providers: [
        { provide: ApiService, useValue: apiSpy },
      ],
      declarations: [
        AppComponent,
        PersonListComponent
      ],
      imports: [NgbModule]
    }).compileComponents();
  }));

  it('should be created', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
